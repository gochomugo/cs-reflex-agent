"""
The environment responsible for providing some minimal platform
for our agent. This is the environment that the agent will
be observing and manipulating.
"""


class Environment:
    def __init__(self):
        # Holds all the objects in the environment.
        self._objs = {}

    def put(self, name, obj):
        """
        Add an object to the environment.
        """
        self._objs[name] = obj
        self.trigger_update(name, 'added to env')
        return self

    def get(self, name):
        """
        Return the environment object with the specified name.
        Otherwise, return None.
        """
        return self._objs.get(name, None)

    def trigger_update(self, entity, message=''):
        """
        Trigger an update in the environment, allowing time in
        the simulation to progress.
        """
        for key in self._objs:
            self._objs[key]._env_sync(self, entity, message=message)

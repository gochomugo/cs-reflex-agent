"""
Our agent. The harvester.
"""


from sets import Set
from .farm import Farm


class Harvester:

    # actions available to the agent
    ACTION_NOOP         = 0x01
    ACTION_HARVEST      = 0x02
    ACTION_MOVE_LEFT    = 0x04
    ACTION_MOVE_RIGHT   = 0x08
    ACTION_MOVE_UP      = 0x10
    ACTION_MOVE_DOWN    = 0x20
    ACTION_MOVE_RETURN  = 0x40
    ACTION_STOP         = 0x80

    def __init__(self, model, position=[0, 0]):
        self.position = position
        self._stopped = False
        self._model = model
        self._state = {
            'visited_partitions': Set(),
            'can_move_left': False,
            'can_move_right': False,
            'can_move_up': False,
            'can_move_down': False,
            'ltr': True,
            'ttb': True,
        }
        self._last_action = Harvester.ACTION_NOOP

    def _env_sync(self, env, entity, message=''):
        # if the agent is stopped, do nothing!
        if self._stopped: return
        # get the farm.
        # if it is not available, the agent can not work
        farm = env.get('farm')
        if not farm: return
        percepts = self._perceive(farm)
        self._update_state(percepts)
        action, label = self._rule_match(percepts)
        self._last_action = action
        self._actuate(farm, action)
        env.trigger_update('harvester', label)

    def _perceive(self, farm):
        """
        Determine percepts.
        """
        percepts = {}
        percepts['partition_is_grown'] = farm.is_partition_grown(self.position)
        return percepts

    def _update_state(self, percepts):
        """
        Update internal state of the agent, using the percepts,
        farm model and already-saved state.
        """
        # when moving from one row to another, we need to
        # switch the directions of agent i.e.
        # from left-to-right to right-to-left (and vice versa), and
        # from top-to-bottom to bottom-to-top (and vice versa)
        if self._last_action & Harvester.ACTION_MOVE_UP:
            self._state['ttb'] = False
            self._state['ltr'] = not self._state['ltr']
        elif self._last_action & Harvester.ACTION_MOVE_DOWN:
            self._state['ttb'] = True
            self._state['ltr'] = not self._state['ltr']

        # we need to track the partitions the agent has visited,
        # so that it can stop when it has visited all of them
        self._state['visited_partitions'].add(self._model.str_position(self.position))

        # which movement actions are available to the agent e.g.
        # the agent can move left, if the model dictates a partition is
        # available should the agent do so
        self._state['can_move_left'] = self._model.has_next_partition(self.position, Farm.MOVE_LEFT)
        self._state['can_move_right'] = self._model.has_next_partition(self.position, Farm.MOVE_RIGHT)
        self._state['can_move_up'] = self._model.has_next_partition(self.position, Farm.MOVE_UP)
        self._state['can_move_down'] = self._model.has_next_partition(self.position, Farm.MOVE_DOWN)

    def _rule_match(self, percepts):
        """
        Return the first matched rule in our condition-action rules i.e.
        returns tuple, (action, label).
        """
        # clearing the farm partition
        if percepts.get('partition_is_grown'):
            return Harvester.ACTION_HARVEST, 'harvest'
        # when to stop the agent
        if len(self._state['visited_partitions']) == self._model.size():
            return Harvester.ACTION_STOP, 'stop'
        # moving from one partition to the next
        # if we are moving from Left to Right:
        if self._state['ltr']:
            if self._state['can_move_right']: return Harvester.ACTION_MOVE_RIGHT, 'move right'
        # if we are moving from Right to Left:
        else:
            if self._state['can_move_left']: return Harvester.ACTION_MOVE_LEFT, 'move left'
        # if we are moving from Top to Bottom:
        if self._state['ttb']:
            if self._state['can_move_down']: return Harvester.ACTION_MOVE_DOWN, 'move down'
        # if we are moving from Bottom to Top
        else:
            if self._state['can_move_up']: return Harvester.ACTION_MOVE_UP, 'move up'
        # the default action is to move to (0,0)
        # this action allows the harvester to be started in any partition
        return Harvester.ACTION_MOVE_RETURN, 'return to (0, 0)'

    def _actuate(self, farm, action):
        """
        Execute the action, from the matched rule.
        """
        # clearing the farm partition
        if action & Harvester.ACTION_HARVEST:
            farm.clear_partition(self.position)
        # moving back to (0, 0)
        if action & Harvester.ACTION_MOVE_RETURN:
            self.position = [0, 0]
        # moving partition to the next
        if action & Harvester.ACTION_MOVE_LEFT:
            self.position[1] = self.position[1] - 1
        if action & Harvester.ACTION_MOVE_RIGHT:
            self.position[1] = self.position[1] + 1
        if action & Harvester.ACTION_MOVE_UP:
            self.position[0] = self.position[0] - 1
        if action & Harvester.ACTION_MOVE_DOWN:
            self.position[0] = self.position[0] + 1
        # stopping
        if action & Harvester.ACTION_STOP:
            self._stopped = True

    def has_visited_partition(self, position):
        """
        Return True if the agent has visited the partition at 'position'.
        Otherwise, return False.
        """
        return self._model.str_position(position) in self._state['visited_partitions']

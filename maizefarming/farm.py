"""
The farm that a harvester can work on.
This stores state information, such as which partitions have
maize crop that can be harvested.
It also provides a model to the harvester that it can use to
understand how the world evolves, such as existence of neighbouring
partitions.
"""


import random


class Farm:

    PARTITION_CLEAR = 0x01
    PARTITION_GROWN = 0x02

    # movement actions
    MOVE_LEFT   = 0x01
    MOVE_RIGHT  = 0x02
    MOVE_UP     = 0x04
    MOVE_DOWN   = 0x08

    def __init__(self, length, width, model=False):
        self.length = length
        self.width = width
        self.model = model
        self._partitions = []
        partition_states = [Farm.PARTITION_CLEAR, Farm.PARTITION_GROWN]
        for lindex in range(self.length):
            self._partitions.append([])
            for windex in range(self.width):
                choice = random.choice(partition_states) if not self.model else Farm.PARTITION_CLEAR
                self._partitions[lindex].append(choice)

    def size(self):
        """
        Return the size of the farm i.e. number of partitions.
        """
        return self.length * self.width

    def has_next_partition(self, position, move):
        """
        Return True if the applying the 'move', with 'position' as the
        current position, ends us on a valid partition.
        Otherwise, return False.
        """
        lindex = position[0]
        windex = position[1]

        if move & Farm.MOVE_RIGHT: windex += 1
        elif move & Farm.MOVE_LEFT: windex -= 1
        elif move & Farm.MOVE_UP: lindex -= 1
        elif move & Farm.MOVE_DOWN: lindex += 1

        if lindex >= self.length or lindex < 0:
            return False
        if windex >= self.width or windex < 0:
            return False
        return True

    def clear_partition(self, position):
        """
        Clear the partition i.e. harvest.
        """
        self._partitions[position[0]][position[1]] = Farm.PARTITION_CLEAR

    def is_partition_grown(self, position):
        """
        Return True if the partition is grown i.e. has maize crop that
        can be harvested.
        Otherwise, return False.
        """
        return self._partitions[position[0]][position[1]] == Farm.PARTITION_GROWN

    def to_model(self):
        """
        Return a model, representing the current farm.
        """
        return Farm(self.length, self.width, model=True)

    def str_position(self, position):
        """
        Return a string representation of the position/coordinates.
        """
        return str(position[0]) + ':' + str(position[1])

    def _env_sync(self, env, entity, message=''):
        pass

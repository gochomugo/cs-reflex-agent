"""
Farm Visualizer. It output a visual depiction of the
environment, in particular, the farm and the agent's
position.
"""


import sys


class FarmVisualizer:
    def __init__(self):
        pass

    def _env_sync(self, env, entity, message=''):
        farm = env.get('farm')
        if not farm: return
        harvester = env.get('harvester')
        if not harvester: return

        output = entity + ': ' + message + '\n'
        def line():
            line = '+'
            for i in range(farm.width):
                line += '----+'
            line += '\n'
            return line
        for lindex in range(farm.length):
            output += line()
            for windex in range(farm.width):
                output += '| '
                position = [lindex, windex]
                in_current_position = harvester.position[0] == lindex and harvester.position[1] == windex
                output += '!' if in_current_position else ' '
                if harvester.has_visited_partition(position) or in_current_position:
                    output += 'G' if farm.is_partition_grown(position) else 'C'
                else:
                    output += ' '
                output += ' '
            output += '|\n'
        output += line()
        print(output)

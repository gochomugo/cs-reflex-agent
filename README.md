# A Model-Based Reflex Agent

> A model-based reflex agent, focussed on harvesting maize crop
> in a farm.


|||
|---|---|
|Student Reg. No|CS281-0666/2013|
|Student Name|Ian Mugo|
|Course|Bsc. Computer Science|
|Unit Name|Knowledge-based Systems|
|Unit Code|ICS 2405|


## running the agent

Ensure you have [Python](https://www.python.org) installed.

In the root directory of the repository:

```bash
$ python run.py
```


## directory structure

The content in this repository is organized as follows:

```
.
├── maizefarming                # module containing environment, agent, etc.
│   ├── environment.py          # environment
│   ├── farm.py                 # farm model
│   ├── farm_visualizer.py      # farm visualizer
│   ├── harvester.py            # farm harvester (agent)
│   └── __init__.py             # module exports
├── README.md                   # documentation
└── run.py                      # program entry point
```


## license:

**The MIT License (MIT)**

Copyright (c) 2016 GochoMugo

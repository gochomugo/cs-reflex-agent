#!/usr/bin/env python


# importing our project
import maizefarming


def main():
    # We create an environment for our agent.
    # It allows agents to perceive and manipulate
    # objects in the environment.
    # It is a representation of the world.
    env = maizefarming.Environment()

    # We create a visualizer for the farm.
    # It will observe the environment and output some
    # useful information to your command-line interface.
    visualizer = maizefarming.FarmVisualizer()
    env.put('visualizer', visualizer)

    # We create our maize farm, sub-divided into several partitions.
    # Maize crop is available in randomly-selected partitions.
    # We add it to our environment to allow agents to discover it.
    farm = maizefarming.Farm(3, 3)
    env.put('farm', farm)

    # We create our harvester (agent) that will work on the farm.
    # Our harvester moves through all farm partitions, harvesting
    # maize crop where available. It stops when all the partitions
    # have been visited.
    # We add it to the environment.
    harvester = maizefarming.Harvester(farm.to_model())
    env.put('harvester', harvester)


# execute the main function if this file is being run
# as a script
if __name__ == '__main__':
    main()
